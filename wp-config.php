<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{b^~zi7OmH^_.p}(_v]EQoK!E?(XE%U>8yp{&L{aesuK<gG$h;ej{nG6-$kjYP|F' );
define( 'SECURE_AUTH_KEY',  '3a-hL:Oiw`F2UfoT`lX&_K`QwO&h+xU]>y7-EuN%@a3z.,nuc8:Z8.3<$W l)L0^' );
define( 'LOGGED_IN_KEY',    '$!LJr#P+6C+H60g|E7t3{8U#?rZj/IVVH_i*XO.L*,p1On-1,m6BG/{+GS}SJ;T[' );
define( 'NONCE_KEY',        '20Y@k6kol|1-Qt=<BLpR0;dQr!wZJ?*F@7-3&-K:>~0u9`Hh$$mnizs2nR.i(&2w' );
define( 'AUTH_SALT',        'g@ Q2!T/iTl{fPRic?/[GKW=6<I?2s:`i8-9%|_,9sIWri9[2`2d~W`Vl_Rjs}4t' );
define( 'SECURE_AUTH_SALT', ',2<<3pxh</9;nIb3:is%!nktBc]mOrljUy03E>UUh4=i)f=v |c`wq=Z^4,-E:JB' );
define( 'LOGGED_IN_SALT',   'X4@rQ]%}o&dih$qr-ii6HLoZ=)$!8m3NHKuRxoMm#iL{k*$SwCs5jh(8nP0EE#37' );
define( 'NONCE_SALT',       'C)[kHwYtO2tg2wg$5?J36^+Pn}CgJs=]A3R 1YBR>,vP63+$eqw%B4Ch9v8%1z]<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
