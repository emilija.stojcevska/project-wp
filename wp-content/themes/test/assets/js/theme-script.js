jQuery(document).ready(function ($) {


    /** =============================================================== **/
    /** Tooltip **/
    /** =============================================================== **/    

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })


    /** =============================================================== **/
    /** Dont close dropdown on click when opened **/
    /** =============================================================== **/    

    $(document).on('click', '.dropdown-menu.dontcloseonclick', function (e) {
        e.stopPropagation();
    });


    /** =============================================================== **/
    /** Menu toogler on mobile hamburger class change **/
    /** =============================================================== **/


    $('.hamburger').on("click", function (e) {
        e.preventDefault();
        $(this).toggleClass('is-active');
    });    

    $('.open-main-menu').on("click", function (e) {
        e.preventDefault();
        $('body').toggleClass('menu-opened');
    });      

    $('.main-menu .close-menu').on("click", function (e) {
        e.preventDefault();
        $('body').removeClass('menu-opened');
    });  

    
});

/** =============================================================== **/
/** Close menu if clicked outside of it **/
/** =============================================================== **/

// ova treba da se prepravi - za ako se klikni nadvor od menito
// $(document).mouseup(function(e) 
// {
//     var container = $(".main-menu");

//     // if the target of the click isn't the container nor a descendant of the container
//     if (!container.is(e.target) && container.has(e.target).length === 0) 
//     {
//         $('body').removeClass('menu-opened');
//     }
// });


