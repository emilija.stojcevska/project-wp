jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
    $(document).on('click', '.buttonControllersPage', function(e){
        e.preventDefault();
        let whatToDo = $(this).attr("data-id");

        if(!$(this).hasClass('disabledLink')){
        let currentPage = $('.currentPage').text();
        currentPage = parseInt($('.currentPage').text());
       
        let maxPage = $('.nextPage').text();
        if(whatToDo == "next"){
            currentPage = parseInt(currentPage)+1;
          }else{
            currentPage = parseInt(currentPage)-1;
          }
       /*  console.log(currentPage);
        console.log(maxPage); */
        
       
        
            
            data = {
                'action': 'loadmore',
                'page' : currentPage,
                
            };
          
    
            $.ajax({ // you can also use $.post here
                url : loadmore_params.ajax_url, // AJAX handler
                data : data,
                type : 'POST',
                whatToDo : whatToDo,
               
                success : function( data ){
                    
                  
                    if( data ) { 
                        
                      $('.align-items-stretch').empty(); // insert new posts
                      $('.align-items-stretch').append(data); 
                     
                      if(currentPage==maxPage){
                          $('.nextBtn').addClass('disabledLink');
                          $('.prevBtn').removeClass('disabledLink');
                      }else if(currentPage == 1){
                        $('.prevBtn').addClass('disabledLink');
                        $('.nextBtn').removeClass('disabledLink');
                      }else{
                        $('.buttonControllersPage').removeClass('disabledLink');
                      }
                        $('.currentPage').text(currentPage);
    
                    } else {
    
                       
                    }
                  
                }

            }); 

        }
            return false;
            
        
    })
})