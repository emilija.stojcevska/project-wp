<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package test
 */

get_header();
?>
<?php $current_category = get_queried_object(); 
/* var_dump($current_category);
 */
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main container-fluid">
		<div class="row">
		<?php
			$products= new HelperFunctions;
			$products = $products->productsCategory('products', $current_category->cat_ID);
			if($products->have_posts()){
				while($products->have_posts()){
					$products->the_post();
					get_template_part('/template-parts/listing', 'products');
				}
			}
			/* var_dump($products); */
		
		;?>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
