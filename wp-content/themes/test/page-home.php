
<?php 

    get_header();
    // Template name: Page Home template
    
?>

<div class="firm-features">
            <div class="container-fluid h-100">
                <div class="d-flex h-100 align-items-center justify-content-between">
                    <div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-uk.svg">Manufactured in the EU</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-badge.svg">10 years experience</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-wood-options.svg">Bespoke options available</div>
                </div>
            </div>
</div>
<?php $image = get_field('first_image');

?>
<div class="top-home" style="background-image: url('<?= $image['url'];?>');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-md-6 mb-5 mb-md-0 text-center text-md-left">
                        <h1 class="poppins mb-3 mb-lg-4"><?= get_field('title_first_section')?></h1>
                        <ul class="list-unstyled mb-3 mb-lg-4">
                            <li><?= get_field('description_1');?></li>
                            <li><?= get_field('description_2');?></li>
                        </ul>
                        <p class="mb-0"><a href="#" class="btn">Get Inspired</a></p>
                    </div>
                    <div class="col-12 col-md-6 text-center text-md-right text-lg-center">
                        <?php 
                            $produc_for_show = new HelperFunctions;
                            $produc_for_show->productForShow('products');
                        ;?>
                    </div>
                </div>
                <div class="bottom">
                    <a href="#" class="scroll-down">SCROLL DOWN</a>
                </div>
            </div>
        </div>


    <!-- end of first image -->



    <div class="boxed">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                        <h3 class="with-border">Browse by PRODUCT</h3>
                        <p> Filter by features, price, size and style requirements. Browse our full collection of engineered flooring or for bespoke requests, please <a href="#"><u>get in touch</u></a></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-3 mb-4 mb-lg-0">
                        <div class="products-div-title">
                            <div class="d-flex align-items-center h-100">
                                <div>Filter<img src="<?= get_template_directory_uri();?>/assets/images/icon-filter-dashes.svg"></div>
                            </div>
                        </div>
                        <div class="product-filters clearfix">
                            <div class="inside">
                                <div class="block">
                                    <label>Style</label>
                                    <select class="">
                                        <option>Any</option>
                                        <option>Style 1</option>
                                        <option>Style 2</option>
                                        <option>Style 3</option>
                                    </select>
                                </div>

                                <div class="block">
                                    <label>Price m<sup>2</sup></label>
                                    <ul class="list-unstyled list-inline mb-0">
                                        <li class="list-inline-item py-1">From</li>
                                        <li class="list-inline-item py-1"><input type="text" name="#" class="form-control v2" placeholder="£0"></li>
                                        <li class="list-inline-item py-1">To</li>
                                        <li class="list-inline-item py-1"><input type="text" name="#" class="form-control v2" placeholder="£31.50"></li>
                                    </ul>
                                </div> 

                                <div class="block">
                                    <label>Thickness</label>
                                    <!-- slider here -->
                                </div> 

                                <div class="block">
                                    <label>ENGINEERED</label>
                                    <select class="">
                                        <option>Any</option>
                                        <option>Style 1</option>
                                        <option>Style 2</option>
                                        <option>Style 3</option>
                                    </select>
                                </div>                            

                                <div class="block">
                                    <label>LAQUER</label>
                                    <select class="">
                                        <option>Any</option>
                                        <option>Style 1</option>
                                        <option>Style 2</option>
                                        <option>Style 3</option>
                                    </select>
                                </div> 

                                <div class="block">
                                    <label>UNDERFLOOR HEATING</label>
                                    <select class="">
                                        <option>Any</option>
                                        <option>Style 1</option>
                                        <option>Style 2</option>
                                        <option>Style 3</option>
                                    </select>
                                </div>

                                <div class="block last">
                                    <a href="#" class="btn btn-block brown">EXPLORE Flooring</a>
                                </div> 
                            </div>                                                                                                                                          
                        </div>
                    </div>
                            <?php
                                $paged = 1;
                                $products = new HelperFunctions;
                                $query = $products->allProductsWithoutShowing('products', $paged, '6');
                               
                            ;?>
                    <div class="col-12 col-lg-9">
                        <div class="products-div-title">
                            <div class="d-flex align-items-start align-items-md-center justify-content-start justify-content-md-between flex-column flex-md-row h-100">
                                <div class="current-pages-info mb-4 mb-md-0">
                                    Showing <strong>6</strong> of 
                                    <strong><?= $query->found_posts ;?></strong></div>
                                <div class="sort-by">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item pt-1">SORT by:</li>
                                        <li class="list-inline-item py-1">
                                            <select class="dark-select">
                                                <option>Most popular</option>
                                                <option>Most popular 1</option>
                                                <option>Most popular 2</option>
                                                <option>Most popular 3</option>
                                            </select>                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- show all products -->
                        <div class="row align-items-stretch">
                            <?php
                                $paged = 1;
                                $products = new HelperFunctions;
                                $query = $products->allProducts('products', $paged, 6);
                              
                               if ( $query ) {
                                    $total = $query->max_num_pages;
                                } else {
                                    $total = $query->max_num_pages;
                                } 
                            ;?>
                                                  
                        </div>

                        <div class="text-center product-nav">
                            <ul class="list-unstyled list-inline">
                                <li class="list-inline-item py-1"><a href="" class="btn btn-sm buttonControllersPage prevBtn disabledLink" data-id="prev">Prev</a></li>
                                <li class="list-inline-item py-1 active currentPage">1</li>
                                <li class="list-inline-item py-1">/</li>
                                <li class="list-inline-item py-1 nextPage"><?= $total;?></li>
                                <li class="list-inline-item py-1"><a href="" class="btn btn-sm buttonControllersPage nextBtn" data-id="next">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="boxed pt-0">
            <div class="container">
                <div class="row text-center mb-4 mb-lg-5">
                    <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                        <h3 class="with-border">Browse by STYLE</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero</a></p>
                    </div>
                </div>                
            	

                <div class="row">
                    
                <?php 
								$args = array('parent' => 6, 'hide_empty' => 0);
                                $categories_style = get_categories( $args );
                                
								
								if(!empty($categories_style)):
                                    foreach($categories_style as $style):
                                        if (function_exists('get_wp_term_image'))
                                        {
                                            $meta_image = get_wp_term_image($style->term_id); 
                                        }

                                        $productsCategory = new HelperFunctions;
                                        $productsCategory = $productsCategory->productsCategory('products', $style->cat_ID);
                                       /*  var_dump($productsCategory); */
								?>
									<div class="col-12 col-md-4 mb-3 mb-md-0">
                                        <div class="product-preview-single style">
                                            <div class="number">
                                                <img src="<?= get_template_directory_uri(); ?>images/icon-style-plank.svg">
                                                <?= $productsCategory->post_count;?> Products
                                            </div>
                                            <div class="image" style="background-image: url('<?= $meta_image;?>');"></div>
                                            <div class="text">
                                                <div class="title mb-2"><?= $style->name;?> </div>
                                                <div class="price mb-2"><?= $style->category_description;?></div>
                                                <div class="links"><a href="<?= get_category_link($style->term_id);?>" class="link">EXPLORE Style</a></div>
                                            </div>                        
                                        </div>
                                    </div>

								<?php 
									endforeach;
								endif;
				?>
                   

                    
                </div>
            </div>
        </div>

        <div class="boxed" style="background-color: #FFF;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-10 offset-lg-1">
                        <h3 class="mb-3 mb-lg-4 mb-lg-5">Request a callback</h3>
                        <div class="row">
                          <!--   <?php echo do_shortcode('[contact-form-7 id="70" title="Contact form 1"]');?> -->
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="First name*">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Last name*">
                                </div>
                            </div>                            
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Contact number*">
                                </div>
                            </div>                            
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email*">
                                </div>
                            </div>                            
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Company">
                                </div>
                            </div>                            
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your message here*"></textarea>
                                </div>
                            </div>                            
                        </div>
                        <div class="d-flex justify-content-between align-items-center flex-column flex-lg-row mt-4 mt-md-0">
                            <div class="mb-4 mb-lg-0">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="test">
                                        <span class="cr"><i class="cr-icon fa fa-check"></i></span> By submitting this form you agree that you have read and understand our <a href="#"><u>Privacy Policy</u></a>
                                    </label>
                                </div>
                            </div>
                            <div><a href="#" class="btn">SEND MESSAGE</a></div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="firm-features bigger">
            <div class="container">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="item"><div class="image"><div class="d-flex h-100 align-items-center justify-content-center"><img src="<?= get_template_directory_uri();?>/assets/images/icon-uk-big.svg"></div></div>Supplying Nationwide</div>
                    <div class="item"><div class="image"><div class="d-flex h-100 align-items-center justify-content-center"><img src="<?= get_template_directory_uri();?>/assets/images/icon-badge-big.svg"></div></div>25+ years experience</div>
                    <div class="item"><div class="image"><div class="d-flex h-100 align-items-center justify-content-center"><img src="<?= get_template_directory_uri();?>/assets/images/icon-wood-options-big.svg"></div></div>Tailored, bespoke options</div>
                    <div class="item"><div class="image"><div class="d-flex h-100 align-items-center justify-content-center"><img src="<?= get_template_directory_uri();?>/assets/images/icon-trophy.svg"></div></div>Leading British manufacturer</div>
                </div>
            </div>
        </div>        


        <?php get_footer();?>