<div class="col-12 col-md-6 mb-3 mb-mb-4">
    <div class="product-preview-single">
        <a href="#" class="add-to-favorite active">
            <svg xmlns="http://www.w3.org/2000/svg" width="17.876" height="15.783" viewBox="0 0 17.876 15.783">
                <path d="M17.436,5.76a4.309,4.309,0,0,0-6.095,0l-.83.83-.83-.83a4.31,4.31,0,1,0-6.095,6.095l.83.83,6.095,6.095,6.095-6.095.83-.83a4.309,4.309,0,0,0,0-6.095Z" transform="translate(-1.573 -3.747)" fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
            </svg>                                        
        </a>
        <div class="image" style="background-image: url('<?= get_the_post_thumbnail_url();?>');"></div>
            <div class="text">
                
            <div class="title mb-1"><?= get_the_title() ;?> </div>
            <div class="price mb-1">Prices from £<?= get_field('price');?> per m2</div>
            <div class="links"><a href="<?= get_the_permalink(); ;?>" class="link">EXPLORE</a></div>
                                    </div>
                                    <div class="details">
                                        <div class="width single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-width.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Width: <?= get_field('width');?>mm</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="thickness single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri() ;?>/assets/images/icon-product-thickness.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Thickness: <?= get_field('thickness');?>mm</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="length single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-lenght.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Length: <?= get_field('length');?>mm</div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="texture single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-texture.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Surface Texture:<br><?= get_field('surface_texture'); ?></div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="finish single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-finish.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Finish:<br><?= get_field('finish');?></div>
                                                </div>
                                            </div>
                                        </div>                                                                                                                                                                
                                    </div>
                                </div>
                            </div>
