

<div class="product-show">
    <div class="title">PRODUCT ON SHOW</div>
        <div class="inside">
            <div class="image" style="background-image: url('<?= get_the_post_thumbnail_url();?>');"></div>
                <div class="name mb-1"><?= get_the_title();?></div>
                    <div class="price mb-0">Prices from £<?= get_field('price');?> per m2</div>
                    <div><a href="<?= get_the_permalink();?>" class="link">EXPLORE Product</a></div>
                </div>
            </div>