

<li class="mb-2">
    <a href="#">
        <span class="image" style="background-image: url(<?= get_the_post_thumbnail_url();?>);"></span>
            <span class="d-flex h-100 align-items-center">
                <span><?= get_the_title();?></span>
            </span>
    </a>
</li>