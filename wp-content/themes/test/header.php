<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package test
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://kit.fontawesome.com/ee4e5ef480.js" crossorigin="anonymous"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'test' ); ?></a>

	

	<header class="d-flex align-items-center justify-content-between">
            <div class="menus h-100">
                <div class="d-flex align-items-center h-100 flex-row flex-wrap">
                    <div class="single h-100">
                        <div class="d-none d-lg-flex h-100 align-items-center">
                            <a href="#" class="open-main-menu hamburger hamburger--collapse">
                                Products
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </a>                            
                        </div>
                        <div class="d-flex d-lg-none h-100 align-items-center">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item open-main-menu" href="#">Products</a>
                                    <?php
								wp_nav_menu( array(
									'theme_location' => 'menu',
									'menu_id'        => 'primary-menu',
								) );
								?>
                                </div>
                            </div>    
                        </div>                    
                    </div> 
                    <div class="single h-100 d-none d-lg-block">
                        <div class="d-flex h-100 align-items-center">
                            <ul class="list-unstyled list-inline mb-0">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu',
									'menu_id'        => 'primary-menu',
								) );
								?>
                            </ul>
                        </div>
                    </div>  
                    
                </div> 
            </div>
            <div class="logo h-100">
                <div class="d-flex align-items-center h-100 flex-row flex-wrap">
                    <a href="<?= esc_url( home_url( '/' ) ); ?>"><img src="<?= get_template_directory_uri();?>/assets/images/circle-logo.png" class="image-auto"></a>
                </div>
            </div>
            <div class="product-social h-100">
                <div class="d-flex align-items-center h-100 flex-row flex-wrap">
                    <div class="single h-100">
                        <div class="d-flex h-100 align-items-center">
                            <ul class="list-unstyled list-inline mb-0">
                                <li class="list-inline-item py-1">
                                    <a href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17.876" height="15.783" viewBox="0 0 17.876 15.783">
                                          <path d="M17.436,5.76a4.309,4.309,0,0,0-6.095,0l-.83.83-.83-.83a4.31,4.31,0,1,0-6.095,6.095l.83.83,6.095,6.095,6.095-6.095.83-.83a4.309,4.309,0,0,0,0-6.095Z" transform="translate(-1.573 -3.747)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                        </svg>
                                        <strong>0</strong>
                                    </a>
                                </li>
                                <li class="list-inline-item py-1"><a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/icon-ionic-ios-search.svg"></a></li>
                                <li class="list-inline-item py-1"><a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/icon-user.svg"></a></li>
                            </ul>
                        </div>
                    </div> 
                    <div class="single h-100 d-none d-lg-block">
                        <div class="d-flex h-100 align-items-center">
                            <ul class="list-unstyled list-inline mb-0">
                                <li class="list-inline-item py-1"><a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/icon-awesome-pinterest-p.svg"></a></li>
                                <li class="list-inline-item py-1"><a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/icon-awesome-twitter.svg"></a></li>
                                <li class="list-inline-item py-1"><a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/icon-awesome-facebook-f.svg"></a></li>
                            </ul>
                        </div>
                    </div>      
                </div>                    
            </div>

            <div class="main-menu">
                <a href="#" class="close-menu">Close <i class="fas fa-times"></i></a>
                <div class="scroll-mobile">
                    <div class="inside d-flex align-items-start align-items-lg-stretch flex-column flex-lg-row flex-wrap">
                        <div class="box collections">
                            <ul class="list-unstyled collections-links mb-0">
								<li class="mb-3 title">Collections</li>
								<?php 
									$args = array('parent' => 5, 'hide_empty' => 0);
									$categories = get_categories( $args );
									if(!empty($categories) && !is_wp_error($categories)):
										foreach($categories as $category):
								?>
									<li class="mb-2"><a href="<?= get_category_link( $category->term_id );?>"><?=$category->name;?></a></li>

								<?php				
										endforeach;
									endif;
								
								;?>
                              
                            </ul> 
                        </div>
                        <div class="box middle">
                            <ul class="list-unstyled mb-0">
								<li class="mb-3 title">By Styles</li>
								<?php 
								$args = array('parent' => 6, 'hide_empty' => 0);
								$categories_style = get_categories( $args );
								
								if(!empty($categories_style)):
									foreach($categories_style as $style):
								?>
									 <li class="mb-2">
										 <a href="<?= get_category_link($style->term_id);?>">
											 <?= $style->name;?>
										 </a>
									</li>

								<?php 
									endforeach;
								endif;
								?>
                              
                            </ul> 

                            <p>&nbsp;</p>

                            <ul class="list-unstyled mb-0">
                                <li class="mb-3 title">By Price</li>
                                <li class="mb-2"><a href="#">£0 - £20 M<sup>2</sup></a></li>
                                <li class="mb-2"><a href="#">£20 - £40 M<sup>2</sup></a></li>
                                <li class="mb-2"><a href="#">£40 - £50 M<sup>2</sup></a></li>
                                <li class="mb-2"><a href="#">£50+ M<sup>2</sup></a></li>
                            </ul>                                                     
                        </div>
                        <div class="box">
                            <ul class="list-unstyled favorited mb-0">
                                <li class="mb-3 title">By Most Favorited</li>
                                <li class="mb-2"><a href="#"><span>1035</span>Mill Lane Plank</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Primrose Drive Herringbone</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Kingsley Wood Road Plank</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Kingsland Road Herringbone</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Orchid Close Parquet</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Pinfold Lane Parquet</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Priory Lane Herringbone</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Roman Road Chevron</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>Highbrown Lane Chevron</a></li>
                                <li class="mb-2"><a href="#"><span>1035</span>BRINDLEY CLOSE</a></li>
                            </ul>                                                    
                        </div>                    
                    </div>
                </div>
            </div>
        </header>

	<div id="content" class="site-content">
