<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package test
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container-fluid">
	<div class="row">
	    <div class="col-md-6">
			<div class="image singleImageProperties" style="background-image: url('<?= get_the_post_thumbnail_url();?>');"></div>
		</div>
		<div class="col-md-6">
            <div class="text">
                
            <div class="title mb-1"><?= get_the_title() ;?> </div>
            <div class="price mb-1">Prices from £<?= get_field('price');?> per m2</div>
            
                                    </div>
                                    <div class="details">
                                        <div class="width single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-width.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Width: <?= get_field('width');?>mm</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="thickness single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri() ;?>/assets/images/icon-product-thickness.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Thickness: <?= get_field('thickness');?>mm</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="length single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-lenght.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Length: <?= get_field('length');?>mm</div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="texture single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-texture.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Surface Texture:<br><?= get_field('surface_texture'); ?></div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="finish single">
                                            <div class="icon h-100">
                                                <div class="d-flex h-100 align-items-center justify-content-center">
                                                    <img src="<?= get_template_directory_uri();?>/assets/images/icon-product-finish.svg">
                                                </div>
                                            </div>
                                            <div class="texxt">
                                                <div class="d-flex h-100 align-items-center">
                                                    <div>Finish:<br><?= get_field('finish');?></div>
                                                </div>
                                            </div>
                                        </div>                                                                                                                                                                
                                    </div>
                                </div>
                            </div>
				</div>
</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
