<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package test
 */

?>

	</div><!-- #content -->
	<footer>
            <div class="container">
                <div class="navigation-footer">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="<?= esc_url( home_url( '/' ) ); ?>"><img src="<?= get_template_directory_uri() ;?>/assets/images/circle-logo.png" height="40"></a></li>
                        <li class="list-inline-item">Homepage</li>
                    </ul>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7 mb-5 mb-lg-0">
                        <div class="title mb-3">Collections</div>
                        <ul class="list-unstyled collections-links">
						<?php 
									$args = array('parent' => 5, 'hide_empty' => 0);
									$categories = get_categories( $args );
									if(!empty($categories) && !is_wp_error($categories)):
										foreach($categories as $category):
								?>
									<li class="mb-2"><a href="<?= get_category_link( $category->term_id );?>"><?=$category->name;?></a></li>

								<?php				
										endforeach;
									endif;
								
								;?>
                        </ul> 
                    </div>
                    <div class="col-12 col-md-6 col-lg-2 mb-5 mb-md-4 mb-lg-0">
                        <div class="title mb-3">Useful Links</div>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a href="about.html">About us</a></li>
                            <li class="mb-2"><a href="#">My Favorites</a></li>
                            <li class="mb-2"><a href="#">Mobile Showroom</a></li>
                            <li class="mb-2"><a href="#">Contact Us</a></li>
                        </ul>                        
                    </div>
                    <div class="col-12 offset-0 col-sm-8 offset-sm-2 col-md-6 offset-md-0 col-lg-3 offset-lg-0">
                        <div class="title mb-2">Recent News</div>
                        <ul class="list-unstyled blogs-links">
							<?php 
								$news = new HelperFunctions;
								$news = $news->showNewsInFooter('news', 1, 3);

							;?>
                                          
                        </ul>
                    </div>
                </div>

                <div class="copy d-flex align-items-center align-items-lg-center justify-content-center justify-content-lg-between flex-wrap flex-column flex-lg-row mt-4 mt-lg-5">
                    <div class="mb-3 mb-lg-0">Copyright © 2019 Test Project. All rights reserved.</div>
                    <div>
                        <ul class="list-unstyled list-inline mb-0">
                            <li class="list-inline-item py-1"><a href="#">Privacy Policy</a></li>
                            <li class="list-inline-item py-1"><a href="#">Use of Cookies</a></li>
                            <li class="list-inline-item py-1"><a href="#">Terms of Use</a></li>
                            <li class="list-inline-item py-1"><a href="#">Sales and Refunds</a></li>
                            <li class="list-inline-item py-1"><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
