<?php


class HelperFunctions{
    function standard_query($postType, $paged=0, $postPerPage){
       
        $args = array(
            'post_type' => $postType,
            'post_status' => 'publish',
            'posts_per_page' => $postPerPage,
            'order_by' => 'data',
            'order' => 'desc',
            'paged' => $paged,
            
            );
        return $args;
    }

    function query_category($postType, $category){
        $args = array(
            'post_type' => $postType,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order_by' => 'data',
            'order' => 'desc',
            'paged' => $paged,
            'cat'=> $category,
            
            );
        return $args;
    }
    function query_product_for_show($postType){
        $args = array(
            'post_type' => $postType,
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'order_by' => 'data',
            'order' => 'desc',
            'meta_query' => array(
                array(
                    'key'     => 'product_on_show',
                    'value'   => '"show"',
                    'compare' => 'LIKE'
                )
            )
            
            );
        return $args;
    }


    public static function allProducts($postType, $paged, $postPerPage){
      
        $args = self::standard_query($postType, $paged, $postPerPage);
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                get_template_part('/template-parts/listing', 'products');
            }
        }

        return $query;

    }
    public static function allProductsWithoutShowing($postType, $paged, $postPerPage){
      
        $args = self::standard_query($postType, $paged, $postPerPage);
        $query = new WP_Query($args);
      
        return $query;

    }

    public static function productsCategory($postType, $category){
        $args = self::query_category($postType, $category);
        $query = new Wp_Query($args);
        return $query;
    }

    public static function productForShow($postType){
        $args = self::query_product_for_show($postType);
        
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                get_template_part('/template-parts/listing', 'product_for_show');
            }
        }
    }

    public static function showNewsInFooter($postType, $paged, $postPerPage){
        $args = self::standard_query($postType, $paged, $postPerPage);
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                get_template_part('/template-parts/listing', 'news');
            }
        }

        return $query;
    }
}