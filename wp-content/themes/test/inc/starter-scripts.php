<?php

function test_scripts() {

    wp_register_style( 'test-style', get_stylesheet_uri() );
    wp_register_style( 'test-bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_register_style( 'test-h-style-style', get_template_directory_uri() . '/assets/css/h-style.css' );
    wp_register_style( 'test-lightbox-style', get_template_directory_uri() . '/assets/css/lightbox.css' );
    wp_register_style( 'test-fonts-style', get_template_directory_uri() . '/assets/css/theme-fonts.css' );
    
    wp_register_style( 'test-theme-style', get_template_directory_uri() . '/assets/css/theme-style.css' );
    wp_register_style( 'test-navbar-style', get_template_directory_uri() . '/assets/css/navbar.css' );
    wp_register_style( 'test-main-style', get_template_directory_uri() . '/assets/css/main.css' );
    


    wp_deregister_script('jquery');

    wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', array(), '20151215' );

    wp_register_script( 'test-bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min', array(), '20151215', true );
    wp_register_script( 'test-lightbox-script', get_template_directory_uri() . '/assets/js/lightbox.js', array(), '20151215', true );

    
    wp_register_script( 'test-custom-script', get_template_directory_uri() . '/assets/js/theme-script.js', array(), '20151215', true );
    wp_register_script( 'loadmore', get_template_directory_uri() . '/assets/js/loadmore.js', array(), '20151215', true ); 

    

    wp_enqueue_script('jquery');

    wp_enqueue_script('test-bootstrap-script');

    wp_enqueue_script('test-lightbox-script');
    wp_enqueue_script('test-custom-script');
    wp_enqueue_script('loadmore');
    


    wp_enqueue_style('test-style');
    wp_enqueue_style('test-bootstrap-style');
    wp_enqueue_style('test-h-style-style');
    wp_enqueue_style('test-lightbox-style');
    wp_enqueue_style('test-theme-fonts-style');
    
    wp_enqueue_style('test-theme-style');
    wp_enqueue_style('test-navbar-style');
    wp_enqueue_style('test-main-style');
   
    wp_localize_script( 'loadmore', 'loadmore_params', array(
        'ajax_url' => admin_url( 'admin-ajax.php' ), // WordPress AJAX
        
    ) );

    
}
add_action( 'wp_enqueue_scripts', 'test_scripts' );

/* function pstarter_admin_scripts(){
    wp_register_style( 'pstarter-admin-style', get_template_directory_uri() . '/assets/css/admin.css' );
    wp_enqueue_style('pstarter-admin-style');
}

add_action( 'admin_enqueue_scripts', 'pstarter_admin_scripts' ); */






